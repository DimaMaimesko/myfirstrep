<?php

class CategoryList{
    
    public static function getCategoryList($number){
        
        $db = Db::getConnection();
        $categoryList = array();
        $result = $db->query('SELECT  * FROM product ORDER BY id ASC LIMIT '."$number".';');
        $i = 0;
        while ($row = $result->fetch()){
            $categoryList[$i]['name'] = $row['name'];
            $categoryList[$i]['price'] = $row['price'];
            $categoryList[$i]['availability'] = $row['availability'];
            $categoryList[$i]['status'] = $row['status'];
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['is_new'] = $row['is_new'];
            $categoryList[$i]['category_id'] = $row['category_id'];
            $i++;
        }
        return $categoryList;
    }
    
    
}

