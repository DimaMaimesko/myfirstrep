<?php

class Product{
     // Количество отображаемых товаров по умолчанию
    const SHOW_BY_DEFAULT = 3;
    /**
     * Возвращает массив последних товаров
     * @param type $count [optional] <p>Количество</p>
     * @param type $page [optional] <p>Номер текущей страницы</p>
     * @return array <p>Массив с товарами</p>
     */
    public static function getLatestProducts($count = self::SHOW_BY_DEFAULT)
    {
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'SELECT id, name, price, is_new FROM product '
                . 'WHERE status = "1" ORDER BY id DESC '
                . 'LIMIT :count';
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);
        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        // Выполнение коменды
        $result->execute();
        // Получение и возврат результатов
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        return $productsList;
    }
   
   public static function getProductById($id)
    {
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'SELECT * FROM product WHERE id = :id';
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);
        // Выполнение коменды
        $result->execute();
        // Получение и возврат результатов
        return $result->fetch();
    } 
    ////////////////////////////////////////////////////////////////////////////////
     public static function updateProductById($id)
    {
        $db = Db::getConnection();
        $sql = 'UPDATE product SET category_id=:category_id,
                code=:code,price=:price,availability=:availability,
                is_new=:is_new,name = :name,brand = :brand,
                is_recommended=:is_recommended,description=:description,
                status=:status,left_pieses=:left_pieses WHERE id=:id;';
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
        $result->bindParam(':name', $_POST["name"], PDO::PARAM_STR);
        $result->bindParam(':category_id', $_POST["category_id"], PDO::PARAM_INT);
        $result->bindParam(':code', $_POST["code"], PDO::PARAM_INT);
        $result->bindParam(':price', $_POST["price"], PDO::PARAM_INT);
        $result->bindParam(':availability', $_POST["availability"], PDO::PARAM_INT);
        $result->bindParam(':brand', $_POST["brand"], PDO::PARAM_STR);
        $result->bindParam(':description', $_POST["description"], PDO::PARAM_STR);
        $result->bindParam(':is_new', $_POST["is_new"], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $_POST["is_recommended"], PDO::PARAM_INT);
        $result->bindParam(':status', $_POST["status"], PDO::PARAM_INT);
        $result->bindParam(':left_pieses',$_POST["left_pieses"] , PDO::PARAM_INT);
        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);
        // Выполнение коменды
        $result->execute();
        // Получение и возврат результатов
        return true;
    } 
    ////////////////////////////////////////////////////////////////////////////
   public static function createProduct($options)
    {
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'INSERT INTO product '
                . '(name, code, price, category_id, brand, availability,'
                . 'description, is_new, is_recommended, status)'
                . 'VALUES '
                . '(:name, :code, :price, :category_id, :brand, :availability,'
                . ':description, :is_new, :is_recommended, :status)';
        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи
            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }

    /////////////////////////////////////////////////////////////////////////////
    public static function getProductsListByCategory($categoryID = false,$page = 1)
    {
        if ($categoryID){
        // Соединение с БД
        $db = Db::getConnection();
        $products = array();
        
        $offset = ($page-1)*self::SHOW_BY_DEFAULT;
        
        $result = $db->query("SELECT * FROM product WHERE status = '1' AND category_id = '$categoryID' ORDER BY id ASC LIMIT ".self::SHOW_BY_DEFAULT." OFFSET "."$offset");
                            
  
        // Получение и возврат результатов
        $i = 0;
       
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['is_new'] = $row['is_new'];
           
            $i++;
        }
        return $products;
    }
    }
    
    public static function getTotalProductsInCategory($categoryID = false){
        
       if ($categoryID){
        // Соединение с БД
        $db = Db::getConnection();
        //$products = array();
        
        $rowTotal = $db->query("SELECT COUNT(id) FROM product WHERE status = '1' AND category_id = '$categoryID';");
         $row = $rowTotal->fetch();
         
        // Получение и возврат результатов
        //$i = 0;
        
       /* while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['is_new'] = $row['is_new'];
           
            $i++;
        }*/
        
        return $row[0];
    }
        
    }
    ///////////////////////////////////////////////////////////////////////////
    public static function getImage($id)
    {
        // Название изображения-пустышки
        $noImage = 'no-image.jpg';
        // Путь к папке с товарами
        $path = '/my_shop/upload/images/';
        // Путь к изображению товара
        $pathToProductImage = $path . $id . '.jpg';
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            // Если изображение для товара существует
            // Возвращаем путь изображения товара
            return $pathToProductImage;
        }
        // Возвращаем путь изображения-пустышки
        return $path . $noImage;
    }
}

