<?php

class Admin{

public static function getAllProducts()
    {
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'SELECT * FROM product WHERE status = "1" ORDER BY id DESC ';
                
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);
        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        // Выполнение коменды
        $result->execute();
        // Получение и возврат результатов
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['left_pieses'] = $row['left_pieses'];
            $i++;
        }
        return $productsList;
    }
    
    public static function deleteProductById($id)
    {
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'DELETE FROM product WHERE id = :id;';
                
        // Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        // Указываем, что хотим получить данные в виде массива
        //$result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        // Выполнение коменды
        //if ($result) return true;
        //else{ echo "Не удалось удалить"; return false;}
        return true;
       
        
       
    }
    
}