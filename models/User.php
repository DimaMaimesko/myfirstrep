<?php

class User{
    
    ////////////////////////////////////////////////////////////////////////////
    public static function register(){
        
       
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function checkName($name){
        if (strlen($name) > 2){
            return true;
        }
        return false;
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function checkEmail($email){
        if (filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;    
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function checkPassword($password){
     if (strlen($password) >= 6){
            return true;
        }
        return false;  
       
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function checkPhone($phone){
        
      $pattern = "/^\+380\d{3}\d{2}\d{2}\d{2}$/";
      if(preg_match($pattern, $phone)) return true;
      else return false; 
  
    }
    
  
    
    public static function checkEmailExist($email){
        //SELECT id FROM `user` WHERE email = "alex@mail.com";
        /* $db = Db::getConnection();
         $row = $db->query("SELECT id FROM user WHERE email =  '$email';");
         $result = $row->fetch();
         return $result;*/
        
        //подготовленный запрос
        $db = Db::getConnection();
        $sql = 'SELECT COUNT(*) FROM user WHERE email = :email';
        $result = $db->prepare($sql);
        $result->bindparam(':email',$email);
        $result->execute();
        
        if ($result->fetchColumn())
            return true;
        return false;
       
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function userWrite($name,$email,$password){
        //INSERT INTO `user`(`name`, `email`, `password`) VALUES ('ddd','ffff','gggg');
        // $db = Db::getConnection();
         //$row = $db->query("INSERT INTO user(name,email,password) VALUES ('$name','$email','$password');");
        // return true;
        // 
         //подготовленный запрос
        $db = Db::getConnection();
        $sql = "INSERT INTO user(name,email,password) VALUES (:name,:email,:password);";
        $result = $db->prepare($sql);
        $result->bindparam(':email',$email);
        $result->bindparam(':name',$name);
        $result->bindparam(':password',$password);
        
        return $result->execute();
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    public static function orderWrite($name,$phone,$message,$products){
        
         //подготовленный запрос
        $db = Db::getConnection();
        //INSERT INTO product_order(user_name,user_phone,user_comment,products) VALUES ('rrrrr','ttttt','ttttt','e');
        $sql = "INSERT INTO product_order(user_name,user_phone,user_comment,products) VALUES (:name,:phone,:comment,:products);";
        $result = $db->prepare($sql);
        $result->bindparam(':phone',$phone);
        $result->bindparam(':name',$name);
        $result->bindparam(':comment',$message);
        $result->bindparam(':products',$products);
        
        return $result->execute();
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function checkUserData($email, $password){
      
        //подготовленный запрос
        $db = Db::getConnection();
        $sql = "SELECT * FROM user WHERE email = :email AND password = :password;";
        $result = $db->prepare($sql);
        $result->bindparam(':email',$email);
        $result->bindparam(':password',$password);
        $result->execute();
        $result = $result->fetch();
        if ($result){
            echo '</br>';
            print_r($result['id']);
            return $result['id'];
        }
         
        return false;
       
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function auth($userId){
        
        $_SESSION['user'] = $userId;
       
    }
    ////////////////////////////////////////////////////////////////////////////
    public static function checkLogged(){
       
        if (isset($_SESSION['user'])){
            return $_SESSION['user'];
        }else return false;
           // require_once(ROOT . '/views/user/login.php');
     
    }
    ////////////////////////////////////////////////////////////////////////////
     public static function getUserNameBuyId($id){
       
        //подготовленный запрос
        $db = Db::getConnection();
        $sql = "SELECT name FROM user WHERE id = :id;";
        $result = $db->prepare($sql);
        $result->bindparam(':id',$id);
      
        $result->execute();
        $result = $result->fetch();
        if ($result){
            //echo '</br>';
           // print_r($result['id']);
            return $result['name'];
        }
         
        return false;
     
    }
    ////////////////////////////////////////////////////////////////////////////
     public static function isAdminById($id){
       
        //подготовленный запрос
        $db = Db::getConnection();
        $sql = "SELECT role FROM user WHERE id = :id;";
        $result = $db->prepare($sql);
        $result->bindparam(':id',$id);
      
        $result->execute();
        $result = $result->fetch();
        if ($result['role'] == 'admin'){
            return true;
        }else return false;
      
    }
    
    
    public static function getUserBuyId($id){
       
        //подготовленный запрос
        $db = Db::getConnection();
        $sql = "SELECT * FROM user WHERE id = :id;";
        $result = $db->prepare($sql);
        $result->bindparam(':id',$id);
      
        $result->execute();
        $result = $result->fetch();
        if ($result){
            //echo '</br>';
           // print_r($result['id']);
            return $result;
        }
         
        return false;
     
    }
    
    public static function getOrder()
    {
              
        if (isset($_SESSION['products'])){
           return  $_SESSION['products'];
        }else return false;
       
    }
   
    
    
}
