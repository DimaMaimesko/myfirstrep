<?php

class CartController
{
    /**
     * Action для страницы просмотра товара
     * @param integer $productId <p>id товара</p>
     */
   
    
    public function actionAdd($productId)
    {
        
        
        Cart::addProduct($productId); 
       
        //$_SESSION['items'] = Cart::countItems();
        
        $referer = $_SERVER['HTTP_REFERER'];
        header ("Location: $referer");
        
       // echo "Это CART".self::$ITEMS;
        return true;
       
       
    }
    ///////////////////////////////////////////////////////////////////////////
    public function actionAddAjax($id)
    {
         
       echo  Cart::addProduct($id); //$_POST["name"];
        //echo "<script> alert(\"{$id}\");</script>";
        return  true;
       
       
    }
    ///////////////////////////////////////////////////////////////////////////
     ///////////////////////////////////////////////////////////////////////////
    public function actionPlusAjax($id)
    {
       Cart::addProduct($id);
       echo Cart::numberOfItemsById($id); 
       return  true;
    }
     ///////////////////////////////////////////////////////////////////////////
    public function actionMinusAjax($id)
    {
      
       if (Cart::numberOfItemsById($id) > 0) Cart::minusProduct($id);
      
       echo Cart::numberOfItemsById($id);
      
       return  true;
    }
    ////////////////////////////////////////////////////////////////////////////
     public function actionTotalAjax($id)
    {
      $product = Product::getProductById($id);
      echo  $product['price'] * Cart::numberOfItemsById($id);
       return  true;
    }
    
    
    public function actionIndex()
    {
        
       // $product = Product::getProductById('34');
     
        //header ("Location: /my_shop/views/cart/index.php");
        require_once ROOT.'/views/cart/index.php';
        //header ("Location: /my_shop/views/cart/cart.html");
        
       // echo "Это CART".self::$ITEMS;
     return true;
    }
    
     public function actionDelete($id)
    {
        
       $productsInCart = array();
       // unset($_SESSION['products']);
        if (isset($_SESSION['products'])){
            $productsInCart = $_SESSION['products'];
            unset($productsInCart[$id]);
            $_SESSION['products'] = $productsInCart;
            //require_once ROOT.'index.php';
        }
        $referer = $_SERVER['HTTP_REFERER'];
        header ("Location: $referer");
        //header ("Location: /my_shop/views/cart/index.php");
    return true;
    }
    
    
}
