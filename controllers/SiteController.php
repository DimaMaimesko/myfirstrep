<?php
include_once ROOT.'/models/Category.php';
include_once ROOT.'/models/Product.php';

class SiteController{
    public function actionIndex()
    {
        // Список категорий для левого меню
        $categories = Category::getCategoriesList();
        // Список последних товаров
        $latestProducts = Product::getLatestProducts(14);
        // Список товаров для слайдера
        //$sliderProducts = Product::getRecommendedProducts();
        // Подключаем вид
        require_once(ROOT . '/views/site/index.php');
        return true;
    }

   
}
