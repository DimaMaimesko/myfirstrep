<?php
include_once ROOT.'/models/Category.php';
include_once ROOT.'/models/Product.php';
include_once ROOT.'/models/CategoryList.php';
include_once ROOT.'/components/Pagination.php';

class CatalogController{
    
   public function actionIndex()
    {
        // Список категорий для левого меню
        $categories = Category::getCategoriesList();
        // Список последних товаров
        $latestProducts = Product::getLatestProducts(12);
        // Подключаем вид
        require_once(ROOT . '/views/catalog/index.php');
        return true;
    }
public function actionCategory($categoryID,$page = 1){
        
        //echo 'Category: '.$categoryID;
       // echo 'Page: '.$page;
    
        $categories = array();
        $categories = Category::getCategoriesList();
        
        $categoryProducts = array();
        $categoryProducts = Product::getProductsListByCategory($categoryID,$page);
        
        $totalNumber = Product::getTotalProductsInCategory($categoryID);
        $pagination  = new Pagination($totalNumber,$page,Product::SHOW_BY_DEFAULT,'page-');
       
        require_once (ROOT.'/views/catalog/category.php');
        return true;
    }

}
