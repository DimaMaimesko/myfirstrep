<?php

class UserController{
    
    public function actionRegister()
    {
       
        $name = '';
        $email = '';
        $password = '';
        $result = false;
        if (isset($_GET["submit"])){
            $name = $_GET["name"];
            $email = $_GET["email"];
            $password = $_GET["password"];
           
            $validateErrors = array();
            
            if (User::checkName($name)){
                echo "<br>"."Name's correct";  
            }else $validateErrors[] = "Name is too short";
            
            if (User::checkEmail($email)){
                echo "<br>"."Email's correct"; 
            }else $validateErrors[] = "Email isn't correct";
            
            if (User::checkPassword($password)){
                echo "<br>"."Password's correct";
            }else $validateErrors[] = "Password is too short";
            
            if (User::checkName($name) && User::checkEmail($email) && User::checkPassword($password)){
              $validateErrors = NULL; 
            }
            if (User::checkEmailExist($email)){
                $validateErrors[] = "Такой email уже существует!!!";
                $result = false;
            }elseif ($validateErrors == NULL){
                $result = User::userWrite($name, $email, $password);
                //session_start();
                $_SESSION['registered'] = true;
                $_SESSION['name'] = $name;
                
            }
            
               
            }
            require_once(ROOT . '/views/user/register.php');
        
       
        
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    public function actionLogin()
    {

        $email = '';
        $password = '';
        $userId = false;
        if (isset($_GET["submitLogin"])){
            $email = $_GET["emailLogin"];
            $password = $_GET["passwordLogin"];
           
            $errors = array();
            
            if (User::checkEmail($email)){
                echo "<br>"."Email's correct"; 
            }else $errors[] = "Email isn't correct";
            
            if (User::checkPassword($password)){
                echo "<br>"."Password's correct";
            }else $errors[] = "Password is too short";
            
           $userId = User::checkUserData($email, $password);
            if ($userId == false){
                $errors[] = 'Неправильные данные для входа на сайт';
            }else{
                $errors[] = $userId;
                User::auth($userId);
                //require_once(ROOT . '/views/cabinet/index.php');
               //echo $_SESSION['user'];
            }
               
            }
        if ($userId){
            require_once(ROOT . '/views/cabinet/index.php');
            
        } else  { 
        require_once(ROOT . '/views/user/login.php');
        }
        
        
        return true;
    }
    
    
    
    
    
 public function actionLogout()
    {      
     unset($_SESSION['user']);
     header("Location: /my_shop/index.php");
      echo 'LOGOUTED'; 
        
       
    }
    
  
   
}