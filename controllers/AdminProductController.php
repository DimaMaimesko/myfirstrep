<?php
class AdminProductController
{
    
 //////////////////////////////////////////////////////////
    public function actionIndex()
    {
       
        $allProducts = Admin::getAllProducts();
    
      echo "YYY";  
    require_once ROOT.'/views/admin/products.php';      
    return true;
        
    }
  //////////////////////////////////////////////////////////////  
    public function actionDelete($id)
    {
       
       echo $id." should be deleted";
      
       if (isset($_POST['submit'])){
           Admin::deleteProductById($id);
            $allProducts = Admin::getAllProducts();
            require_once ROOT.'/views/admin/products.php';
        }else require_once ROOT.'/views/admin/delete.php';  
        
    return true;
    }
   ///////////////////////////////////////////////////////////////// 
    
     public function actionUpdate($id)
    {
       
       echo $id." should be updated";
       
        
        if (isset($_POST["submit"])){
           
            //$id = Product::createProduct($options);
          // $options['image'] = $_FILES['image'];

          //  echo '<pre>';            print_r($options['image']); die();
            Product::updateProductById($id);
            
           if ($id){
             if(is_uploaded_file($_FILES["image"]["tmp_name"])){
                move_uploaded_file($_FILES["image"]["tmp_name"],$_SERVER['DOCUMENT_ROOT']."/my_shop/upload/images/{$id}.jpg");  
             } 
           }else {echo "Не удалось создать запись в базу";die();};
        }
          
       $product = Product::getProductById($id);
    require_once ROOT.'/views/admin/update.php';      
    return true;
        
    }
    ///////////////////////////////////////////////////////////////////////
     public function actionCreate($id)
    {
      $categoriesList = Category::getCategoriesList();
      if (isset($_POST["submit"])){
          // Если форма отправлена
            // Получаем данные из формы
            $options['name'] = $_POST['name'];
            $options['code'] = $_POST['code'];
            $options['price'] = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['availability'] = $_POST['availability'];
            $options['description'] = $_POST['description'];
            $options['is_new'] = $_POST['is_new'];
            $options['is_recommended'] = $_POST['is_recommended'];
            $options['status'] = $_POST['status'];
            //$options['image'] = $_FILES['image'];

           // echo '<pre>';            print_r($options['image']); die();
            
           $id = Product::createProduct($options);
           if ($id){
             if(is_uploaded_file($_FILES["image"]["tmp_name"])){
                move_uploaded_file($_FILES["image"]["tmp_name"],$_SERVER['DOCUMENT_ROOT']."/my_shop/upload/images/{$id}.jpg");  
             } 
           }else {echo "Не удалось создать запись в базу";die();};
           
            $allProducts = Admin::getAllProducts();
           require_once ROOT.'/views/admin/products.php';
        }else {require_once ROOT.'/views/admin/create.php';}
        
          
    return true;
        
    }
    ///////////////////////////////////////////////////////////////////////       
      
}


