<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Главная</title>
        <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
        <link href="/my_shop/template/css/animate.css" rel="stylesheet">
        <link href="/my_shop/template/css/main.css" rel="stylesheet">
        <link href="/my_shop/template/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="/my_shop/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/my_shop/template/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        
<?php include ROOT.'/views/layouts/header.php';?>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Каталог</h2>
                            <div class="panel-group category-products">
                                
                                <?php foreach ($categories as $categoryItem): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="/my_shop/category/<?php echo $categoryItem['id'];?>"
                                               class=" <?php if ($categoryID == $categoryItem['id']) echo 'active';  ?>"
                                               > 
                                            <?php echo $categoryItem['name'];?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                                <?php endforeach;?>
                                
                              
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-9 padding-right">
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">Последние товары</h2>
                            
                            <?php foreach ($categoryProducts as $product): ?>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="/my_shop/template/images/home/product<?php echo $product['id'];?>.jpg" alt="" />
                                            <h2> <?php echo $product['price'].'$'?> </h2>
                                            <p>
                                                <a href="/my_shop/product/<?php echo $product['id'];?>" >
                                                     <?php echo "ID=".$product['id'].": ".$product['name'];?> 
                                                </a> 
                                            </p>
                                              <a href="/my_shop/cart/<?php echo $product['id'];?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                        </div>
                                        <?php if ($product['is_new']): ?>
                                        <img src="/my_shop/template/images/home/new.png" class="new" alt="" />
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            
                       
                      <?php endforeach;?>
 </div>
                    
                     <?php echo $pagination->get(); ?>
                </div>
            </div>
        </section>

        
<?php include ROOT.'/views/layouts/footer.php';?>


        <script src="/my_shop/template/js/jquery.js"></script>
        <script src="/my_shop/template/js/bootstrap.min.js"></script>
        <script src="/my_shop/template/js/jquery.scrollUp.min.js"></script>
        <script src="/my_shop/template/js/price-range.js"></script>
        <script src="/my_shop/template/js/jquery.prettyPhoto.js"></script>
        <script src="/my_shop/template/js/main.js"></script>
    </body>
</html>



