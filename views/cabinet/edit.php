
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Главная</title>
        <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
        <link href="/my_shop/template/css/animate.css" rel="stylesheet">
        <link href="/my_shop/template/css/main.css" rel="stylesheet">
        <link href="/my_shop/template/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="template/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/my_shop/template/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        
 <?php include ROOT.'/views/layouts/header.php';?>

	<section id="form"><!--form-->
		<div class="container">
                    <div class="row">
                      <?php if(faLSE):?>
                        <h2>Вы зарегестрированы!</h2>
                                            
                        <?php else: ?>
                      <?php if (isset($validateErrors)) echo "<h4>"."List of errors"."</h4>"; ?>
                      <?php if (isset($validateErrors) && is_array($validateErrors)):?>
                      
                        <ul><?php foreach($validateErrors as $err):
                          if ($err){
                              echo "<li>".$err."</li>";
                          }
                          endforeach;?>
                        </ul>
                      
                        
                    <?php endif; ?>   -->
                    </div>
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h1>Редактирование данных!</h1>
                                                <form action="#" method="GET">
							<input type="text" name="nameNew" placeholder="Name" value="<?php echo $name;?>"/>
							<input type="email" name="emailNew" placeholder="Email Address"  value="<?php echo $email;?>"/>
							<input type="password" name="passwordNew" placeholder="Password" value="<?php echo $password;?>"/>
							<input type="submit" name ="submitNew" value="СОХРАНИТЬ" class="btn btn-default">
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		 <?php endif; ?>
                </div>
	</section><!--/form-->
        
        
        
                
<?php include 'C:\wamp6\www\MY_SHOP/views/layouts/footer.php';?>


       
    </body>
</html>

