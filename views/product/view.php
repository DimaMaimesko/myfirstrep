<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Товар</title>
        <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
        <link href="/my_shop/template/css/animate.css" rel="stylesheet">
        <link href="/my_shop/template/css/main.css" rel="stylesheet">
        <link href="/my_shop/template/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="/my_shop/template/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/my_shop/template/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <?php include ROOT.'/views/layouts/header.php';?>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Каталог</h2>
                            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                
                                <?php foreach ($categories as $categoryItem): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="/my_shop/category/<?php echo $categoryItem['id'];?>"> 
                                            <?php echo $categoryItem['name'];?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                                <?php endforeach;?>
                              
                                       
                                </div>
                            </div><!--/category-products-->

                        </div>
                    </div>

                    <div class="col-sm-9 padding-right" style="float:right;">
                        <div class="product-details"><!--product-details-->
                            <div class="row" style="float:right;">
                                <div class="col-sm-5">
                                    <div class="view-product">
                                        <img src="/my_shop/template/images/product-details/1.jpg" alt="" />
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="product-information"><!--/product-information-->
                                        <img src="/my_shop/template/images/product-details/new.jpg" class="newarrival" alt="" />
                                        <h2><?php echo $product['name'];?></h2>
                                        <p><?php echo $product['code'];?></p>
                                        <span>
                                            <span>US $<?php echo $product['price'];?></span>
                                            <label>Количество:</label>
                                            <input type="text" value= '87' />
                                            <button type="button" class="btn btn-fefault cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                В корзину
                                            </button>
                                        </span>
                                        <p><b>Наличие:</b> На складе</p>
                                        <p><b>Состояние:</b> Новое</p>
                                        <p><b>Производитель:</b> D&amp;G</p>
                                    </div><!--/product-information-->
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-sm-12">
                                    <h5>Описание товара</h5>
                                    <p><?php echo $product['description'];?></p>
                                </div>
                            </div>
                        </div><!--/product-details-->

                    </div>
                </div>
            </div>
        </section>
        

        <br/>
        <br/>
        
        <?php include ROOT.'/views/layouts/footer.php';?>



        <script src="/my_shop/template/js/jquery.js"></script>
        <script src="/my_shop/template/js/price-range.js"></script>
        <script src="/my_shop/template/js/jquery.scrollUp.min.js"></script>
        <script src="/my_shop/template/js/bootstrap.min.js"></script>
        <script src="/my_shop/template/js/jquery.prettyPhoto.js"></script>
        <script src="/my_shop/template/js/main.js"></script>
    </body>
</html>

