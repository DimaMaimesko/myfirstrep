
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li id="tel"><a  href="#"><i class="fa fa-phone"></i> +38 093 000 11 22</a></li>
                                    <li id="adr"><a  href="#"><i class="fa fa-envelope"></i> zinchenko.us@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li id="upr1"><a href="#"><i class="fa fa-facebook" title="I'm FACEBOOK"></i></a></li>
                                    <li id="upr2"><a href="#"><i class="fa fa-google-plus" title="I'm GOOGLE"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="index.php"><img src="template/images/home/logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">  
          
                                    
                                     <li><a href="/my_shop/cart/index/"><i class="fa fa-shopping-cart"></i> Корзина
                                           (<span id="cart-countAdj"><?php echo Cart::countItems(); ?></span>)
                                        </a></li>   
                                   <?php if (isset($_SESSION['user'])) echo '<li><a href="/my_shop/canet/"><i class="fa fa-user"></i> Аккаунт</a></li>';?>
                                    <?php if (!isset($_SESSION['user'])) echo '<li><a href="/my_shop/login/"><i class="fa fa-lock"></i> Login</a></li>';?>
                                    <li><a href="/my_shop/register/"><i class="fa fa-lock"></i> SignUp</a></li>
                                   <?php if (isset($_SESSION['user'])) echo '<li><a href="/my_shop/logout/"><i class="fa fa-unlock"></i> LogOut</a></li>';?>
                                    <li><?php if (isset($_SESSION['user']))
                                        {$name = User::getUserNameBuyId($_SESSION['user']);
                                        echo 'Wellcome, '."$name".'!';}
                                        else echo 'Wellcome, GUEST!';
                                        ?></li>
                                    <li><?php if (isset($_SESSION['user'])){
                                                 if(User::isAdminById($_SESSION['user'])){
                                                     echo '<li><a href="/my_shop/admin/"><i class="fa fa-eur" aria-hidden="true"></i>АдминПанель</a></li>';
                                                 }else echo 'You are not admin!';
                                              }
                                             
                                        ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="/my_shop/index.php">Главная</a></li>
                                    <li class="dropdown"><a href="#">Магазин<i class="fa fa-angle-down"></i></a>
                                        <ul role="menu" class="sub-menu">
                                            <li><a href="/my_shop/catalog/index.php">Каталог товаров</a></li>
                                            <li><a href="/my_shop/cart">Корзина</a></li> 
                                        </ul>
                                    </li> 
                                    <li><a href="/my_shop/blog/">Блог</a></li> 
                                    <li><a href="/my_shop/about/">О магазине</a></li>
                                    <li><a href="/my_shop/contact/">Связаться с нами</a></li>
                                    
                                    <li class="dropdown"> <a href="#">Магазин<i class="fa fa-angle-down"></i></a>  
                                    <ul role="menu" class="sub-menu">
                                      <li><a href="#">HTML</a></li>
                                      <li><a href="#">CSS</a></li>
                                      <li><a href="#">JavaScript</a></li>
                                    </ul>
                                    </li>
                                    
                                    
                                  
                                    
                                </ul>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div><!--/header-bottom-->
            
</header><!--/header-->



