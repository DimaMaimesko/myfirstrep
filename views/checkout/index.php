<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Checkout | E-Shopper</title>
    <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
    <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
    <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
    <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
    <link href="/my_shop/template/css/animate.css" rel="stylesheet">
	<link href="/my_shop/template/css/main.css" rel="stylesheet">
	<link href="/my_shop/template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="/my_shop/template/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		
	<?php require_once('C:\wamp6\www\MY_SHOP/components/Autoload.php');?>
	<?php require_once  'C:\wamp6\www\MY_SHOP/views/layouts/header.php';?>
            
	</header><!--/header-->

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div><!--/checkout-options-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						<div class="shopper-info">
							<p>Shopper Information</p>
							
								<form action="#" method="POST">
                                                            <input type="text" name="name" placeholder="User Name" value="<?php echo $preName;?>">
                                                            <input type="phone" name="phone" placeholder="PhoneNumber">
                                                            <textarea name="message" name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
							<input type="submit" name ="submit" class="btn btn-default">
                                                        </form>
							
						</div>
					</div>
					<div class="col-sm-5 clearfix">
						
					</div>
					<div class="col-sm-4">
                                             <div class="row">
                    
                      <?php if (isset($errors) && is_array($errors)):?>
                     
                        <ul><?php foreach($errors as $err):
                          if ($err){
                              echo "<li>".$err."</li>";
                          }
                          endforeach;?>
                        </ul>
                      
                        
                    <?php endif; ?>   
                       
                        
                        
                   
                    </div>
                                            
                                            
						
					</div>					
				</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
                                           
    <?php $products = array();
if (isset($_SESSION['products'])){
    $productsInCart = $_SESSION['products'];
    foreach ($productsInCart as $key => $value) {
     $products[] = Product::getProductById($key);
     }
    } ?>               
                                            
                                             <?php foreach ($products as $key => $value): ?>

                                            <?php //$productInCart = array(); ?>
                                             <?php $productInCart = $products[$key]; ?>
						<tr>
							<td class="cart_product">
								<a href=""><img src="/my_shop/template/images/cart/one.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""></a><?php echo $productInCart['name'];?></h4>
								<p><?php<?php echo $productInCart['code'];?>?></p>
							</td>
							<td class="cart_price">
								<p><?php echo $productInCart['price'].'$';?></p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $productsInCart[$productInCart['id']]; ?>" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price"><?php $total = $productInCart['price']*$productsInCart[$productInCart['id']]; echo $total.'$';?></p>
							</td>
							<td class="cart_delete">
                                                            <a class="cart_quantity_delete" href="/my_shop/delete/<?php echo $productInCart['id']; ?>"><i class="fa fa-times"></i></a>
							</td>
						</tr>
                                             
                                            <?php endforeach;?>
						
					</tbody>
				</table>
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->

	
<?php require_once  'C:\wamp6\www\MY_SHOP/views/layouts/header.php';?>


    <script src="/my_shop/template/js/jquery.js"></script>
	<script src="/my_shop/template/js/bootstrap.min.js"></script>
    <script src="/my_shop/template/js/jquery.scrollUp.min.js"></script>
    <script src="/my_shop/template/js/jquery.prettyPhoto.js"></script>
    <script src="/my_shop/template/js/main.js"></script>
</body>
</html>
