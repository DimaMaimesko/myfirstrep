<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Главная</title>
        <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
        <link href="/my_shop/template/css/animate.css" rel="stylesheet">
        <link href="/my_shop/template/css/main.css" rel="stylesheet">
        <link href="/my_shop/template/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="/my_shop/template/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/my_shop/template/images/ico/apple-touch-icon-57-precomposed.png">
    <link href="/my_shop/my_style.css" rel="stylesheet">
    
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script>
           $(document).ready(function(){
               
                $(".add-to-cart").click(function () {
                         var id = $(this).attr("data-id");
                         $.post("/my_shop/cartAjax/"+id,
                               {name: "Donald Duck",},
                               function (data) {
                                   $("#cart-countAdj").text(data); 
                                                }
                         );
                         return false;
                });
            });
          
        </script>  
    </head><!--/head-->

    <body>
    
        
        
<?php include ROOT.'/views/layouts/header.php';?>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Каталог</h2>
                            <div class="panel-group category-products">
                                
                                <?php foreach ($categories as $categoryItem): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="/my_shop/category/<?php echo $categoryItem['id'];?>"> 
                                            <?php echo $categoryItem['name'];?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                                <?php endforeach;?>
                                
                              
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-9 padding-right">
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">Последние товары</h2>
                            
                            <?php foreach ($latestProducts as $categoryItem): ?>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                           
                                            
                                          
                                          <a href="/my_shop/product/<?php echo $categoryItem['id'];?>" target="_blank">
                                            <img id="photo" src="/my_shop/upload/images/<?php echo $categoryItem['id'];?>.jpg" alt=""/>
                                           </a>
                                         
                                            
                                            <h2> <?php echo $categoryItem['price'].'$'?> </h2>
                                            <p>
                                                <a id="nameStyle" href="/my_shop/product/<?php echo $categoryItem['id'];?>" >
                                                     <?php echo $categoryItem['name'];?> 
                                                </a> 
                                            </p>
                                              <a href='#' class="btn btn-default add-to-cart" data-id="<?php echo $categoryItem['id'];?>" ><i class="fa fa-shopping-cart"></i>В корзину</a>
                                        </div>
                                        <?php if ($categoryItem['is_new']): ?>
                                        <img src="/my_shop/template/images/home/new.png" class="new" alt="" />
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach;?>

                        </div><!--features_items-->

                        <div class="recommended_items"><!--recommended_items-->
                            <h2 class="title text-center">Рекомендуемые товары</h2>

                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">	
                                        
                                        
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">
                                                        <img src="/my_shop/template/images/home/recommend1.jpg" alt="" />
                                                        <h2 id="xx">$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            
                                            
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">
                                                        <img src="/my_shop/template/images/home/recommend2.jpg" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">
                                                        <img src="/my_shop/template/images/home/recommend3.jpg" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">	
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">
                                                        <img src="/my_shop/template/images/home/recommend1.jpg" alt="" />
                                                        <h2 id="r1">$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">
                                                        <img src="/my_shop/template/images/home/recommend2.jpg" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">
                                                        <img src="/my_shop/template/images/home/recommend3.jpg" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>			
                            </div>
                        </div><!--/recommended_items-->

                    </div>
                </div>
            </div>
        </section>

        
<?php include ROOT.'/views/layouts/footer.php';?>

<script>
document.getElementById("r1").innerHTML = "Hellcx xo World!";
document.title = "DIMA";

document.getElementById("xx").innerHTML = "WWWWWWWW";
$(function(){
 $("#upr1").click(function(){
        $("#tel").hide();
     });
   // jQuery methods go here...
$("#upr2").click(function(){
        $("#tel").show();
     });
});

</script>
        <script src="/my_shop/template/js/jquery.js"></script>
        <script src="/my_shop/template/js/bootstrap.min.js"></script>
        <script src="/my_shop/template/js/jquery.scrollUp.min.js"></script>
        <script src="/my_shop/template/js/price-range.js"></script>
        <script src="/my_shop/template/js/jquery.prettyPhoto.js"></script>
        <script src="/my_shop/template/js/main.js"></script>
    </body>
</html>


