<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Главная</title>
        <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
        <link href="/my_shop/template/css/animate.css" rel="stylesheet">
        <link href="/my_shop/template/css/main.css" rel="stylesheet">
        <link href="/my_shop/template/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="template/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/my_shop/template/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        
<?php include ROOT.'/views/layouts/header.php';?>

	<section id="form"><!--form-->
		<div class="container">
                    <div class="row">
                      
                    </div>
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2><?php echo "Редактирование товара ".$product['id'].":";?></h2>
                                                <form action="#" method="post" enctype="multipart/form-data">
							<p>Id<input type="text" name="id"  value="<?php echo $product['id'];?>"/></p>
							<p>name<input type="text" name="name"  value="<?php echo $product['name'];?>"/></p>
							<p>category_id<input type="text" name="category_id"  value="<?php echo $product['category_id'];?>"/></p>
							<p>code<input type="text" name="code"  value="<?php echo $product['code'];?>"/></p>
							<p>availability<input type="text" name="availability"  value="<?php echo $product['availability'];?>"/></p>
							<p>brand<input type="text" name="brand"   value="<?php echo $product['brand'];?>"/></p>
							<p>description<input type="message" rows="8" cols="50" name="description" value="<?php echo $product['description'];?>" /></p>
                                                       
							<p>is_new<input type="text" name="is_new"  value="<?php echo $product['is_new'];?>"/></p>
							<p>is_recommended<input type="text" name="is_recommended" value="<?php echo $product['is_recommended'];?>"/></p>
							<p>status<input type="text" name="status"  value="<?php echo $product['status'];?>"/></p>
							<p>left_pieses<input type="text" name="left_pieses"  value="<?php echo $product['left_pieses'];?>"/></p>
							<p>price<input type="text" name="price"  value="<?php echo $product['price'];?>"/></p>
							
                                                       
                                                         
                                                          <p>Изображение товара</p>
                                                        <img src="<?php echo Product::getImage($product['id']); ?>" width="200" alt="" />
                                                        <input type="file" name="image" placeholder="" value="<?php echo Product::getImage($product['id']);?>">
                                                         
                                                        
                                                        <input type="submit" name ="submit" class="btn btn-default">
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		
                </div>
	</section><!--/form-->
        
        
        
                
<?php include ROOT.'/views/layouts/footer.php';?>


       
    </body>
</html>
