<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Главная</title>
        <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
        <link href="/my_shop/template/css/animate.css" rel="stylesheet">
        <link href="/my_shop/template/css/main.css" rel="stylesheet">
        <link href="/my_shop/template/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="/my_shop/template/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/my_shop/template/images/ico/apple-touch-icon-57-precomposed.png">
   
<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            
            <br/>
            
            <h4>Добрый день, администратор!</h4>
            
            <style>
table {
    width:100%;
}  
table, th, td {
    
    border-style: double;
    
}
th.right, td.right {
    padding: 5px;
    text-align: right;
}
th.center, td.center {
    padding: 5px;
    text-align: center;
}

table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#fff;
}
table#t01 th {
    background-color: black;
    color: white;
}

table#t01 tr:hover {background-color: #ff00ff}
</style>



 <a href="/my_shop/adminProduct/create/77" class="btn btn-default back"><i class="fa fa-plus"></i> Добавить товар</a>
<table id="t01">
  <tr>
    <th>ID товара</th>
    <th>Артикул</th> 
    <th>Название</th>
    <th>На складе</th>
    <th>Цена</th>
    <th>Редактировать</th>
    <th>Удалить</th>
    
   
  </tr>
  <?php foreach ($allProducts as $product): ?>
  <tr>
   
    <td class="center"><?php echo $product['id']; ?></td>
    <td class="right"><?php echo $product['code']; ?></td>
    <td class="right"><?php echo $product['name']; ?></td>
    <td class="right"><?php echo $product['left_pieses']; ?></td>
    <td class="center"<?php if ($product['price']<= 100) echo ' style="background-color:red"; title="Распродажа" '?>><?php echo $product['price']; ?></td>
    <td class="center"><a href="/my_shop/adminProduct/update/<?php echo $product['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
    <td class="center"><a href="/my_shop/adminProduct/delete/<?php  echo $product['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
  
  </tr>
  <?php endforeach;?>
</table>



 





  <br/><br/><br/><br/><br/><br/><br/>          
            
            
            
            
            
          
                   
            </ul>
            
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

        <script src="/my_shop/template/js/jquery.js"></script>
        <script src="/my_shop/template/js/price-range.js"></script>
        <script src="/my_shop/template/js/jquery.scrollUp.min.js"></script>
        <script src="/my_shop/template/js/bootstrap.min.js"></script>
        <script src="/my_shop/template/js/jquery.prettyPhoto.js"></script>
        <script src="/my_shop/template/js/main.js"></script>
    </body>
</html>
