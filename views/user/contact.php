<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Contact | E-Shopper</title>
    <link href="/my_shop/template/css/bootstrap.min.css" rel="stylesheet">
    <link href="/my_shop/template/css/font-awesome.min.css" rel="stylesheet">
    <link href="/my_shop/template/css/prettyPhoto.css" rel="stylesheet">
    <link href="/my_shop/template/css/price-range.css" rel="stylesheet">
    <link href="/my_shop/template/css/animate.css" rel="stylesheet">
	<link href="/my_shop/template/css/main.css" rel="stylesheet">
	<link href="/my_shop/template/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/my_shop/template/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/my_shop/template/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/my_shop/template/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/my_shop/template/images/ico/apple-touch-icon-57-precomposed.png">

<link href="/my_shop/my_style.css" rel="stylesheet">
 <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script>
           
          $(document).ready(function(){
       /* var $allElements = $('#name');*/       
       /* console.log($allElements);*/
                $("span#submit_message.error").css("border","0px solid greenyellow");
                $("#name").blur(function () {
                        $.post("/my_shop/contactAjax/",
                               {name: $("#name").val(),
                                partial: 1},
                               function (data) {
                               if (data[0][1]){ $("#error_name").text(data[0][0]).css("color", "red"); $("#name").css("background-color", "red"); }
                               else{$("#error_name").text(""); $("#name").css("background-color", "yellow");}
                               },
                               "json"
                         );
                });
                
                 $("#subject").blur(function () {
                        $.post("/my_shop/contactAjax/",
                               {subject: $("#subject").val(),
                               partial: 1},
                               function (data) {
                               if (data[1][1]){ $("#error_subject").text(data[1][0]).css("color", "red"); $("#subject").css("background-color", "red");}
                               else{$("#error_subject").text(""); $("#subject").css("background-color", "yellow");}
                               },
                               "json"
                         );
                });
                
                 $("#email").blur(function () {
                        $.post("/my_shop/contactAjax/",
                               {email: $("#email").val(),
                               partial: 1},
                               function (data) {
                                if (data[2][1]){ $("#error_email").text(data[2][0]).css("color", "red"); $("#email").css("background-color", "red");}
                               else{$("#error_email").text(""); $("#email").css("background-color", "yellow");}
                                },
                               "json"
                         );
                });
                
                 $("#message").blur(function () {
                        $.post("/my_shop/contactAjax/",
                               {message: $("#message").val(),
                               partial: 1},
                               function (data) {
                                if (data[3][1]){ $("#error_message").text(data[3][0]).css("color", "red"); $("#message").css("background-color", "red");}
                               else{$("#error_message").text(""); $("#message").css("background-color", "yellow");}
                                },
                               "json"
                         );
                }); 
                $("#submit").click(function () {
               /* $("#contact-form").submit(function(event){*/
                    event.preventDefault();
                    $("span#submit_message.error").css("border","7px solid greenyellow");
                     $("span#submit_message.error").slideDown(1000);
                     
                     
                    $.post("/my_shop/contactAjax/",
                               {name: $("#name").val(),
                                subject: $("#subject").val(),
                                email: $("#email").val(),
                                message: $("#message").val(),
                                partial: 0},
                            
                               function (data) {
                                   console.log(data);
                                     if (data[0][1]){ $("#error_name").text(data[0][0]); $("#name").css("background-color", "red"); }
                                      else{$("#error_name").text(""); $("#name").css("background-color", "yellow");}
                                      if (data[1][1]){ $("#error_subject").text(data[1][0]); $("#subject").css("background-color", "red");}
                                      else{$("#error_subject").text(""); $("#subject").css("background-color", "yellow");}
                                      if (data[2][1]){ $("#error_email").text(data[2][0]); $("#email").css("background-color", "red");}
                                      else{$("#error_email").text(""); $("#email").css("background-color", "yellow");}
                                      if (data[3][1]){ $("#error_message").text(data[3][0]); $("#message").css("background-color", "red");}
                                      else{$("#error_message").text(""); $("#message").css("background-color", "yellow");}
                              
                               
                                      if (data[4][1]){ $("#submit_message").text(data[4][0]); $("#submit_message").css("background-color", "red").css("color", "black");}
                                      else{$("#submit_message").text(data[4][0]);
                                          $("#submit_message").css("background-color", "yellow");
                                          $("#message,#subject,#email,#name").css("background-color", "yellow");
                                          $("#message,#subject,#email,#name").val("");
                                          }
                               },
                               "json"
                         );
               });
               
                $("#reset").click(function () {
                    event.preventDefault();
                    $("#submit_message").slideUp(1000);
                    $("span#submit_message.error").css("border","0px solid greenyellow");
                    $("#message,#subject,#email,#name").css("background-color", "yellow");
                    $("#message,#subject,#email,#name").val("");
                    $("#error_message,#error_subject,#error_email,#error_name").text("");
                   
                 });
        });     
               
            
    </script>  

</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<?php include ROOT.'/views/layouts/header.php';?>
	</header><!--/header-->
	 
	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Contact <strong>Us</strong></h2>    			    				    				
					
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Get In Touch</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="contact-form"  class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" id="name"  value="" class="form-control"  placeholder="Name">
                                                <span class="error" id="error_name"></span>
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" id="email" value="" class="form-control"  placeholder="Email">
                                                 <span class="error" id="error_email"></span>
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" id="subject" value="" class="form-control"  placeholder="Subject">
                                                <span class="error" id="error_subject"></span>
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" value="" class="form-control" rows="12" placeholder="Your Message Here"></textarea>
                                                 <span class="error" id="error_message"></span>
                                            </div>                        
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="Submit">
				                <input type="reset" name="reset" id ="reset" class="btn btn-primary pull-right" value="Reset">
                                                <div id="error"> <span class="error" id="submit_message"></span></div>    
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Contact Info</h2>
	    				<address>
	    					<p>E-Shopper Inc.</p>
							<p>935 W. Webster Ave New Streets Chicago, IL 60614, NY</p>
							<p>Newyork USA</p>
							<p>Mobile: +2346 17 38 93</p>
							<p>Fax: 1-714-252-0026</p>
							<p>Email: info@e-shopper.com</p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Social Networking</h2>
							<ul>
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-youtube"></i></a>
								</li>
							</ul>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->
	
	<footer id="footer"><!--Footer-->
		
	<?php include ROOT.'/views/layouts/footer.php';?>	
	</footer><!--/Footer-->
	

  
    <script src="/my_shop/template/js/jquery.js"></script>
	<script src="/my_shop/template/js/bootstrap.min.js"></script>
	
    <script type="text/javascript" src="js/gmaps.js"></script>
	<script src="/my_shop/template/js/contact.js"></script>
	<script src="/my_shop/template/js/price-range.js"></script>
    <script src="/my_shop/template/js/jquery.scrollUp.min.js"></script>
    <script src="/my_shop/template/js/jquery.prettyPhoto.js"></script>
    <script src="/my_shop/template/js/main.js"></script>
</body>
</html>
