<?php

return array(
    // Товар:
    
    'adminProduct/delete/([0-9]+)' => 'adminProduct/delete/$1',
    'adminProduct/update/([0-9]+)' => 'adminProduct/update/$1',
    'admin/product' => 'adminProduct/index',
    
    'adminProduct/create/([0-9]+)' => 'adminProduct/create/$1',
    'admin' => 'admin/index',
    'product/([0-9]+)' => 'product/view/$1', // actionView в ProductController
    // Каталог:
    'catalog' => 'catalog/index', // actionIndex в CatalogController
    'register' => 'user/register', // actionRegister в UserController
    'cabinet2/edit' => 'cabinet2/edit', // actionRegister в UserController
    'canet' => 'canet/index', // actionRegister в UserController
    
    
    
    'cartAjax/([0-9]+)' => 'cart/addAjax/$1',
    'cartAjaxPlus/([0-9]+)' => 'cart/PlusAjax/$1',
    'cartAjaxMinus/([0-9]+)' => 'cart/MinusAjax/$1',
    'cartTotalAjax/([0-9]+)' => 'cart/TotalAjax/$1',
    
    
    'cart/index' => 'cart/index',
    'cart/([0-9]+)' => 'cart/add/$1',
    'delete/([0-9]+)' => 'cart/delete/$1',
    'checkout' => 'checkout/index',
    
     'contactAjax' => 'contact/check',
     'contact' => 'contact/index',
    
    'login' => 'user/login', // actionRegister в UserController
    'logout' => 'user/logout', // actionRegister в UserController
    // Категория товаров:
     'edit' => 'user/edit',
    'category/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2', // actionCategory в CatalogController
    'category/([0-9]+)' => 'catalog/category/$1', // actionCategory в CatalogController
    // Главная страница
    'index.php' => 'site/index', // actionIndex в SiteController
    '' => 'site/index', // actionIndex в SiteController
    
   
    
    
   // 'category/([0-9]+)' => 'catalog/category/$1',//
   // 'product/([0-9]+)' => 'product/view/$1',// actionView в ProductController
   // 'catalog' => 'catalog/index',// 
     
    
    //'index.php' => 'site/index',// actionIndex в SiteController
);
