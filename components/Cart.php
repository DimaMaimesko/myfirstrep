<?php

class Cart{
    
    
    public static function addProduct($id){
        $id = intval($id);
        
        $productsInCart = array();
      
        if (isset($_SESSION['products'])){
            $productsInCart = $_SESSION['products'];
        }
        
        if (array_key_exists($id, $productsInCart)){
            $productsInCart[$id]++;
        }else{
            $productsInCart[$id] = 1;
        }
        
        $_SESSION['products'] = $productsInCart;
        //echo '<pre>';        print_r($_SESSION['products']);        die();
        return self::countItems();
    }  
    
    public static function minusProduct($id){
        $id = intval($id);
        
        $productsInCart = array();
      
        if (isset($_SESSION['products'])){
            $productsInCart = $_SESSION['products'];
        }
        
        if (array_key_exists($id, $productsInCart)){
            $productsInCart[$id]--;
        }else{
            $productsInCart[$id] = 0;
        }
        
        $_SESSION['products'] = $productsInCart;
        //echo '<pre>';        print_r($_SESSION['products']);        die();
        return self::countItems();
    }    
    
    ////////////////////////////////////////////////////////////////////////////
    public static function countItems(){
       
        if (isset($_SESSION['products'])){
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity) {
                     $count = $count + $quantity;
             }
             return $count;
        }return 0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    public static function numberOfItemsById($id){
         
        if (isset($_SESSION['products'])){
            $productsInCart = $_SESSION['products'];
          
           $number = $productsInCart[$id];
           
             return $number;
        }return 0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    public static function clearCart(){
       
    unset($_SESSION['products']);
          
    }
    
    ////////////////////////////////////////////////////////////////////////////
}

